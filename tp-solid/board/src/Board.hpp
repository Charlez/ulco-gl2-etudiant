#pragma once

#include "Itemable.hpp"


class Board : public Titleable {
    private:
        std::vector<std::string> _tasks;

    public:
        Board() {}

        void add(const std::string & t) {
            _tasks.push_back(t);
        }

        std::vector<std::string> getItems() const override {
            return _tasks;
        }

        virtual std::string getTitle() const override {
            return "Board";
        }
};

class NumBoard : public Board {
    private:
        int _n;
    
    public:
        NumBoard() : _n(1) {}

        void numAdd(const std::string & t) {
            add(std::to_string(_n) + ". " + t);
            _n++;
        }

        virtual std::string getTitle() const override {
            return "NumBoard";
        }
};
