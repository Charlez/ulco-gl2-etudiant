#pragma once

#include "Itemable.hpp"

class Reportable {
    public:
        virtual ~Reportable() = default;
        virtual void report(const Itemable & itemable) = 0;
};