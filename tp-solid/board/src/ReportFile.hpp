#pragma once

#include "Itemable.hpp"
#include "Reportable.hpp"

#include <fstream>
#include <iostream>

class ReportFile : public Reportable {
    private:
        std::ofstream _ofs;

    public:
        //ReportFile(const std::string & filename) : _ofs(filename) {}
        void report(const Itemable& items) {
            for (const std::string & item : items.getItems())
                _ofs << item << std::endl;
            _ofs << std::endl;
        }
}