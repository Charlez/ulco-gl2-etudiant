
#include "Board.hpp"
#include "Reportable.hpp"
#include "ReportStdout.hpp"

void testBoard(Board & b, Reportable & r) {
    //ReportStdout res;
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");
    //res.reportStdout(b);
    r.report(b);
}

void testNumBoard(NumBoard & b, Reportable & r) {
    //ReportStdout res;
    std::cout << b.getTitle() << std::endl;
    b.numAdd("item 1");
    b.numAdd("item 2");
    //res.reportStdout(b);
    r.report(b);
}

int main() {

    Board b1;
    ReportStdout r1;
    testBoard(b1, r1);

    NumBoard b2;
    ReportStdout r2;
    testNumBoard(b2, r2);

    return 0;
}

