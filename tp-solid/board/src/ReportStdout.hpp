#pragma once

#include "Itemable.hpp"
#include "Reportable.hpp"

#include <fstream>
#include <iostream>

class ReportStdout : public Reportable {
    public:
        void report(const Itemable& items) {
            for (const std::string & item : items.getItems())
                std::cout << item << std::endl;
            std::cout << std::endl;
        };
};