"use strict";

module.exports = {
	repeatN: repeatN
}

function repeatN (n, f) {
	console.log(f.name + "(" + n + ") = " + f(n));
}
