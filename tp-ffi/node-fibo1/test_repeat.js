"use strict";

const repeat = require("./repeat.js")
const fibo1 = require("./fibo1")


for (let i=0; i<10; i++) {
	repeat.repeatN(i, fibo1.fiboIterative);
}
