"use strict";

const sinus = require("./sinus");

console.log('sinus.sinus(1, 0, 0.5) =', sinus.sinus(1, 0, 0.5));
console.log('sinus.sinus(1, 0, 0.25 =', sinus.sinus(1, 0, 0.25));