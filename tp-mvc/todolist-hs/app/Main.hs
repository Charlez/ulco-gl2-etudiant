import Board
import View

-- import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b
    putStrLn ""
    input <- getLine
    let (action:phrase) = words input
    case action of
        "add" -> do
            let (_, b') = addTodo (unwords phrase) b
            loop b'
        "done" -> do
            putStrLn $ "Tache terminee: " ++ (unwords phrase)
            loop b
        "quit" -> putStrLn "Au revoir !"
        _ -> loop b
    


main :: IO ()
main = loop newBoard

