module View where
    
import Board
import Task

showTask :: Task -> String
showTask t = show (_taskId t) ++ ". " ++ (_taskName t)

printBoard :: Board -> IO ()
printBoard b = do
    putStrLn "Todo:"
    let todoList = _boardTodo b
    mapM_ (putStrLn . showTask) todoList
    putStrLn "Done:"
    let doneList = _boardDone b
    mapM_ (putStrLn . showTask) doneList
