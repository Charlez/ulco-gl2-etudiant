#pragma once

#ifndef _BOARD_HPP_
#define _BOARD_HPP_

#include "task.hpp"

#include <iostream>
#include <vector>


struct Board {
    int _boardID;
    std::vector<Task> _boardTodo;
    std::vector<Task> _boardDone;

    Board();

    void addTodo(std::string str);

    void toDone(int n);
};


#endif