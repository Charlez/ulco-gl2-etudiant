#pragma once

#ifndef _VIEWS_HPP_
#define _VIEWS_HPP_

#include "task.hpp"
#include "board.hpp"

#include <iostream>


std::string showTask(Task t);

void printBoard(Board b);


#endif