#pragma once

#ifndef _TASK_HPP_
#define _TASK_HPP_

#include <iostream>


struct Task {
    int _taskID;
    std::string _taskName;

    Task(int id, std::string name):
        _taskID(id), _taskName(name) {}
};


#endif