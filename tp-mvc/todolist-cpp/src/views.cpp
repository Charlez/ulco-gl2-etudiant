#include "views.hpp"

#include <iostream>
#include <string>
#include <algorithm>


std::string showTask(Task t) {
    return (std::to_string(t._taskID) + ". " + t._taskName);
}

void printBoard (Board b) {
    std::cout << "Todo: " << std::endl;
    for_each(b._boardTodo.begin(), b._boardTodo.end(), showTask);
    std::cout << "Done: " << std::endl;
    for_each(b._boardDone.begin(), b._boardDone.end(), showTask);
}
